<?php
include_once("../lib/date.php");
// Config options
$inform = "treasurer@sucs.org";
$permission = "sucsstaff";
// Enable and disable database updating
$enable = TRUE;

// Set next payment date
$paydate = paidUntil(time());

// Only staff can use this page
if (isset($session->groups[$permission])) {
    $smarty->assign("staff", TRUE);
//	$sucsDB->debug = true;

    // handle updates
    if (isset($_POST['uid']) && isset($_POST['lastupdate'])) {
        // Check data hasn't changed and that nothing is broked
        $query = "SELECT * FROM members WHERE uid=? AND lastupdate=?";
        $array = array($_POST['uid'], $_POST['lastupdate']);
        $data = $sucsDB->GetAll($query, $array);

        // If there is only one record then everything is fine
        if (sizeof($data) == 1) {
            // get info on currently logged in user
            $user = posix_getpwnam($session->username);

            // Update record
            $record = $data[0];
            $query = "UPDATE members";
            $query .= " SET paid = ?, lastupdate = DEFAULT, lastedit = ?";
            $query .= " WHERE uid = ?";
            $array = array($paydate, $user['uid'], $_POST['uid']);
            if ($enable) {
                $sucsDB->query($query, $array);
            }

            // emailing contact (tresurer)
            $message = "Account renewal notification\n\n";
            $message .= "Account   : " . $record['username'] . "\n";
            $message .= "User Type : " . $record['type'] . "\n";
            $message .= "Renewed by: " . $user['name'] . "\n\n";
            $message .= "Regards\n  eclipse's script";
            mail($inform, "Account Renewal", $message);

            // emailing user
            $message = "Your Swansea University Computer Society (SUCS) membership has been renewed\n\n";
            $message .= "Username: " . $record['username'] . "\n";
            $message .= "If you do not know or have forgotten your password, please email admin@sucs.org to arrange for it to be changed.\n\n";
            $message .= "Regards\n  The SUCS admin";
            $header = "From: admin@sucs.org\r\n";
            $header .= "Reply-To: admin@sucs.org";
            // Personal account
            mail($record['email'], "SUCS account renewal", $message, $header);
            // sucs account
            mail($record['username'] . "@sucs.org", "SUCS account renewal", $message, $header);

            message_flash("Renewed account for: " . htmlentities($record['username']));
        } else {
            trigger_error("Number of recored returned: " . sizeof($data) . ". Expected: 1.", E_USER_ERROR);
        }
    }

    // if sort is specified in GET
    if (isset($_GET["sort"])) {
        $sortoptions = array("username", "sid", "realname");
        // and is a valid option
        if (in_array($_GET["sort"], $sortoptions, TRUE)) {
            // use it
            $sort = $_GET["sort"];
        } // else use username
        else {
            $sort = 'username';
        }
        $getsort = $sort;
    } //else use username
    else {
        $sort = 'username';
    }


    //Get members details
    $query = "SELECT * FROM members, member_type";
    $query .= " WHERE paid != ?";
    $query .= " AND (type = 1 OR type = 2 OR type = 5)";
    $query .= " AND type=member_type.id";
    $query .= " ORDER BY paid,type," . $sort;
    $array = array($paydate);
    $data = $sucsDB->GetAll($query, $array);
    $smarty->assign("members", $data);

    // set refresh rate
    $autorefresh = $_GET["autorefresh"];
    // if autorefresh is not 'n'
    if ($autorefresh != "n") {
        // and is a decimal value
        if (ctype_digit($autorefresh)) {
            // use it
            $refreshval = $autorefresh;
            // set passthrough
            $getrefresh = $refreshval;
        } else {
            // If no valid value assigned, default to OFF
            $refreshval = "n";
        }
        $optrefresh = $refreshval;
        $smarty->assign("refresh", $refreshval);
    } else {
        // set passthrough
        $getrefresh = 'n';
        $optrefresh = 'n';
    }

    // compile passthrough url
    // sort
    if (isset($getsort)) {
        $getout = "?sort=" . $getsort;
    }

    // autorefresh
    if (isset($getrefresh)) {
        if (isset($getout)) {
            $getout .= "&amp;autorefresh=" . $getrefresh;
        } else {
            $getout = "?autorefresh=" . $getrefresh;
        }
    }

    // set smarty variables
    $smarty->assign("self", $baseurl . $path . $getout);
    $smarty->assign("optionrefresh", $optrefresh);
    $smarty->assign("optionsort", $sort);
    $smarty->assign("paydate", $paydate);

}
$side = $smarty->fetch('membershiprenew-options.tpl');
$body = $smarty->fetch('membershiprenew.tpl');
$smarty->assign('secondary', $side);
$smarty->assign('title', "Renew Membership");
$smarty->assign('body', $body);
?>
