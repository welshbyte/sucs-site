{* Banana awarding widget *}

<div class="cbb">
    {if $awarded == true}
        <h3>Nomination Received</h3>
        <p>Thanks for your nomination. A member of our elite <q>banana squad</q> will review it soon.</p>
        <p>
            <small>Please note that not all nominations will necessarily become awards</small>
        </p>
    {else}
        {if $banana_admin == true}
            {assign var='action_text' value='Award'}
            <h3>Award Bananas to {$who}</h3>
        {else}
            {assign var='action_text' value='Nominate'}
            <h3>Nominate {$who} for a Banana Award</h3>
            <p><q>Please tell us if you think this user has done something good recently (deserving yellowy recognition)
                    or something dumb (deserving of greenish derision)</q></p>
        {/if}
        <form action="{$url}" method="post" id="bananaform">
            <div>
                <input type="hidden" name="action" value="award"/>

                <p>Number of bananas</p>
                <select name="number">
                    <option>3</option>
                    <option>2</option>
                    <option selected="selected">1</option>
                    <option>-1</option>
                    <option>-2</option>
                    <option>-3</option>
                </select>
            </div>
            <div>
                <p>Why</p>
                <span class="textinput"><textarea id="bananareason" name="why" formid="bananaform" cols="25" rows="10"></textarea></span>
            </div>
            <div>
                <span class="textinput"><input type="submit" value="{$action_text}"/></span>
            </div>
            <div class="clear"></div>
        </form>
    {/if}
</div>
