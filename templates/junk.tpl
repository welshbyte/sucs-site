{if $mode == 'nojunk'}
    {include file="../static/fragments/Junk.txt"}
    <div class="box">
        <div class="boxhead">
            <h3>No Junk</h3>
        </div>
        <div class="boxcontent">
            <h4>There is currently no junk</h4>
            <h4></h4>
        </div>
        <div class="hollowfoot">
            <div>
                <div></div>
            </div>
        </div>
    </div>
    {if $admin}
        <div class="edit">
            <a href="{$componentpath}/Add">Add</a>

            <div class="clear"></div>
        </div>
    {/if}
{/if}
{if $mode == 'list'}
    {include file="../static/fragments/Junk.txt"}
    {foreach name=junk from=$junk key=status item=statusitems}
        <div class="box">
            <div class="boxhead">
                {if $status == 'available'}
                    <h3>Free Stuff!</h3>
                {elseif $status == 'requested'}
                    <h3>Reserved Items</h3>
                {elseif $status == 'taken'}
                    <h3>Recently Taken</h3>
                {else}
                    <h3>Not Junk</h3>
                {/if}
            </div>
            <div class="boxcontent">
                {foreach from=$statusitems item=category key=categoryname}
                    <h4>{$categoryname}</h4>
                    <ul>
                        {foreach from=$category item=item}
                            <li>
                                <strong>{$item.title}:</strong> {$item.description}
                                {if $item.donated_by != null}
                                    <br/>
                                    <small>Donated by {$item.donated_by}</small>
                                {/if}
                                {if $status == 'requested'}
                                    <br/>
                                    <small>Requested by {$item.requested_by}
                                        on {$item.requested_on|date_format:"%e %b %Y"}</small>
                                {elseif $status  == "taken"}
                                    <br/>
                                    <small>Taken by {$item.requested_by}
                                        on {$item.taken_on|date_format:"%e %b %Y"}</small>
                                {/if}
                                {if $session->loggedin}
                                    {if $admin || $item.status=='junk'}
                                        <form action="{$url}" method="POST">
                                            <input type="hidden" name="item" value="{$item.id}"/>
                                            {if $item.status=='junk'}
                                                {if $item.requested_by==null}
                                                    <input type="submit" name="action" value="Request"/>
                                                {elseif ($item.requested_by==$session->username || $admin) && $item.taken_on==null}
                                                    <input type="submit" name="action" value="Un-Request"/>
                                                {/if}
                                                {if $admin}
                                                    {if $item.requested_by!=null && $item.taken_on==null}
                                                        <input type="submit" name="action" value="Item Taken"/>
                                                    {elseif $item.requested_by==null}
                                                        <input type="submit" name="action" value="Not Junk"/>
                                                        <input type="submit" name="action" value="Remove"/>
                                                    {/if}
                                                {/if}
                                            {elseif $admin}
                                                <input type="submit" name="action" value="Junk"/>
                                            {/if}
                                        </form>
                                    {/if}
                                {/if}
                                {if $admin && $item.requested_by==null}
                                    <div class="edit">
                                        <a href="{$componentpath}/Edit/{$item.id}">Edit</a>

                                        <div class="clear"></div>
                                    </div>
                                {/if}
                            </li>
                        {/foreach}
                    </ul>
                {/foreach}
            </div>
            <div class="hollowfoot">
                <div>
                    <div></div>
                </div>
            </div>
        </div>
    {/foreach}
    {if $admin}
        <div class="edit">
            <a href="{$componentpath}/Add">Add</a>

            <div class="clear"></div>
        </div>
    {/if}
{elseif ($mode == 'edit' || $mode == 'add') && $admin}
    <fieldset>
        <legend>{if $mode=='edit'}Edit{else}New{/if} Item</legend>
        <form action="{$componentpath}" method="POST">
            <input type="hidden" name="id" value="{$item.id}"/>

            <div class="row">
                <label for="title">Title*</label>
                <span class="textinput"><input type="text"
                                               name="title"{if $mode == 'edit'} value="{$item.title}"{/if} /></span>
            </div>
            <div class="row">
                <label for="category">Category*</label>
		<span class="textinput">
			<select name="categorymenu">
                <option value="">Other...</option>
                {foreach name=categories from=$categories item=category}
                    <option{if ($category == $item.category) && ($mode == 'edit')} selected="selected"{/if}>{$category}</option>
                {/foreach}
            </select>
		</span> 
		<span class="textinput">
			<div class="note">If none of the existing categories apply, enter one below:</div>
			<input type="text" name="category"/>
		</span>
            </div>
            <div class="row">
                <label for="description">Description</label>
                <span class="textinput"><textarea name="description" cols="25"
                                                  rows="3">{if $mode == 'edit'}{$item.description}{/if}</textarea></span>
            </div>
            <div class="row">
                <label for="donated_by">Donated by</label>
                <span class="textinput"><input type="text"
                                               name="donated_by"{if $mode == 'edit'} value="{$item.donated_by}"{/if} /></span>
            </div>
            <div class="row">
                <label for="status">Status</label>
		<span class="textinput">
			<select name="status">
                {foreach name=statuses from=$statuses item=status}
                    <option{if ($status == $item.status) && ($mode == 'edit')} selected="selected"{/if}>{$status}</option>
                    {foreachelse}
                    <option>unknown</option>
                {/foreach}
            </select>
		</span>
            </div>
            <div class="row">
                <span class="textinput"><input type="submit" name="{if $mode == 'edit'}update{else}add{/if}"
                                               value="{if $mode == 'edit'}Update{else}Add{/if}"/></span>

                <div class="note row">* denotes required fields</div>
            </div>
            <div class="clear"></div>
        </form>
    </fieldset>
{/if}
