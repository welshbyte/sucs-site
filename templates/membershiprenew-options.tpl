{if $staff == TRUE}
    <div class="cbb">
        <h3>Settings</h3>

        <form method="get" action="{$baseurl}{$path}">
            <h4>Sort</h4>

            <p>Select a sort column from the list:</p>
            <select name="sort">
                <option value="username"{if $optionsort == "username"} selected="selected"{/if}>Username</option>
                <option value="sid"{if $optionsort == "sid"} selected="selected"{/if}>Student Number</option>
                <option value="realname"{if $optionsort == "realname"} selected="selected"{/if}>Real Name</option>
            </select>
            <h4>Auto Refresh</h4>

            <p>Enter a value in seconds or n to disable:</p>
            <input class="text" type="text" name="autorefresh" value="{$optionrefresh}" size="5"/><br/>
            <input class="submit" type="submit" value="Update"/>
        </form>
    </div>
{/if}
