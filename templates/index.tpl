
{if $secondary}
<div id="primary">
    {/if}
    <div id="primaryC">
        {$body}

        {* {if $creatable}
            <p>You can <a href="?action=create">create</a> this page though.</p>
        {/if} *}

        {if $modified}
            <div class="edit">
                {* {if $editable}
                    <ul>
                        <li><a href="?action=delete-query">Delete</a></li>
                        <li><a href="?action=edit">Edit</a></li>
                    </ul>
                    <div class="clear"></div>
                {/if} *}
                Page last modified{if $modifiedby} by <a
                href="{$baseurl}/Community/Members/{$modifiedby}">{$modifiedby}</a>{/if} on {$modified}
            </div>
        {/if}
    </div>
    {if $secondary}
</div>

    <div id="secondary">
        {foreach from=$secondary item=widget}
            {$widget}
        {/foreach}
    </div>
{/if}
