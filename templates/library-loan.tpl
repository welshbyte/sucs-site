<div class="cbb">
    <h3>Book Status</h3>
    {if $book.onloan==true }
        {if $librarian==true}
            <p>This book is currently on loan to {$book.loanwho}</p>
            <form action="{$url}" method="POST">
                <input type="hidden" name="action" value="return"/>
                <input type="submit" value="Return"/>
            </form>
        {else}
            <p>This book is currently on loan</p>
        {/if}
    {elseif $librarian==true}
        <p>This book is available</p>
        <form action="{$url}" method="POST">
            <input type="hidden" name="action" value="loan"/>

            <p>Loan to user</p>
            <select name="member">
                {foreach name=members from=$memberlist item=member}
                    <option value="{$member.uid}">{$member.uid}</option>
                {/foreach}
            </select>
            <input type="submit" value="Loan"/>
        </form>
    {elseif $checkout_request==true}
        <p>You have requested to take out {$book.title} from the library.</p>
        <p>You will be contacted by a member of our admin team when the book is available for collection.</p>
    {else}
        <p>This book is available</p>
        <p>You may request to take it out of the library</p>
        <form action="{$url}" method="POST">
            <input type="hidden" name="action" value="loan"/>
            <input type="submit" value="Request"/>
        </form>
    {/if}
</div>
