<table>
    {assign var=lastday value=''}
    {foreach from=$taglist item=row name=list}
        {if $row.day != $lastday}
            {assign var=lastday value=$row.day}
            <tr>
                <td nowrap="nowrap" class="date" colspan="3">{$row.added|date_format:'%d %b %Y'}</td>
            </tr>
        {/if}
        <tr>
            <td nowrap="nowrap" class="time">{$row.added|date_format:'%H:%M:%S'}</td>
            <td nowrap="nowrap" class="user">{$row.user}</td>
            <td>{$row.line|escape:'htmlall'}</td>
        </tr>
    {/foreach}
</table>
