<div class="box">
    <div class="boxhead"><h2>{$book.title}</h2></div>
    <div class="boxcontent">

        <strong>{if $book.onloan}On loan{else}Book Available{/if}</strong>
        {if $book.image_url != ""}<img class="emblem" src="{$book.image_url|escape}" alt="{$book.title|escape}" />{/if}
        {if $editable == true}
            <form class="admin" action="{$baseurl}{$path}?action=save" method="POST">
                <div class="row">
                    <label for="title">Title</label>
                    <span class="textinput"><input type="text" name="title" value="{$book.title}"/></span>
                </div>
                <div class="row">
                    <label for="author">Author</label>
                    <span class="textinput"><input type="text" name="author" value="{$book.author}"/></span>
                </div>
                <div class="row">
                    <label for="publisher">Publisher</label>
                    <span class="textinput"><input type="text" name="publisher" value="{$book.publisher}"/></span>
                </div>
                <div class="row"><label for="description">Description</label><textarea cols="50" rows="20"
                                                                                       name="description">{$book.description|escape}</textarea>
                </div>
                <input type="submit" value="Save"/>
            </form>
        {else}
            <p>You don't have permission to edit books</p>
        {/if}
        <div class="clear"></div>
    </div>
    <div class="hollowfoot">
        <div>
            <div></div>
        </div>
    </div>
</div>
