# The main sucs website

<VirtualHost 137.44.10.1:80>
	DocumentRoot "/var/www/sucssite/htdocs"
	ServerAdmin admin@sucs.org
	ServerName sucs.org
	ServerAlias www.sucs.org
	ServerAlias sucs.swan.ac.uk
	ServerAlias www.sucs.swan.ac.uk
	ServerAlias compsoc.swan.ac.uk
	ServerAlias www.compsoc.swan.ac.uk

	Include /etc/apache2/conf.sucs.d/userdir.conf

	ErrorLog /var/log/apache2/sucssite_error.log
	CustomLog /var/log/apache2/sucssite_access.log combined
	LogLevel warn

	<Directory "/var/www/sucssite/htdocs">
		AllowOverride All

		XBitHack On

		Order allow,deny
		Allow from all
		

		Options +MultiViews
		<IfModule mod_rewrite.c>
			RewriteEngine On

			# Ensure requests are for our preferred domains

			# Disabled by ~imranh 08/04/2015
			# We want people to do to sucs.org becuase we have a ssl cert for that
			# Keep stuff thats come in via *.swan.ac.uk in .ac.uk
			#RewriteCond %{HTTP_HOST} ^www.sucs.swan.ac.uk [NC,OR]
			#RewriteCond %{HTTP_HOST} ^compsoc.swan.ac.uk [NC,OR]
			#RewriteCond %{HTTP_HOST} ^www.compsoc.swan.ac.uk [NC]
			#RewriteRule ^(.*)$ http://sucs.swan.ac.uk/$1 [R=301,L]

			#(web)mail.sucs.org to sucs.org/webmail
			RewriteCond %{HTTP_HOST} ^mail.sucs.org [NC,OR]
			RewriteCond %{HTTP_HOST} ^webmail.sucs.org [NC]
			RewriteRule ^(.*)$ http://sucs.org/webmail/ [R=301,L]

			# Everything else may as well goto sucs.org
			RewriteCond %{HTTP_HOST} !^sucs.org [NC]
			#RewriteCond %{HTTP_HOST} !^sucs.swan.ac.uk [NC]
			RewriteRule ^(.*)$ http://sucs.org/$1 [R=301,L]

			# If its not a file or directory pass it to the website script
			RewriteCond %{REQUEST_URI} !=/server-status
			RewriteCond %{REQUEST_FILENAME} !-f
			RewriteCond %{REQUEST_FILENAME} !-d
			RewriteCond %{REQUEST_URI} !^/index\.php.*
			RewriteRule ^(.*)$ index.php/$1 [QSA,L]
		</IfModule>
		#RedirectMatch permanent ^/Community/Forum$ https://sucs.org/Community/Forum/
		RedirectMatch permanent ^/webmail$ https://sucs.org/webmail/
	</Directory>
</VirtualHost>

<IfModule mod_ssl.c>
<VirtualHost 137.44.10.1:443>
	DocumentRoot "/var/www/sucssite/htdocs"
	ServerAdmin admin@sucs.org
	ServerName sucs.org
	ServerAlias www.sucs.org
	ServerAlias sucs.swan.ac.uk
	ServerAlias www.sucs.swan.ac.uk
	ServerAlias compsoc.swan.ac.uk
	ServerAlias www.compsoc.swan.ac.uk

	ErrorLog /var/log/apache2/ssl_error.log
	TransferLog /var/log/apache2/ssl_access.log
	LogLevel warn

	SSLEngine on

	SSLCipherSuite EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH
	SSLProtocol All -SSLv2 -SSLv3
	SSLHonorCipherOrder On
	Header always set Strict-Transport-Security "max-age=604800"
	Header always set X-Frame-Options SAMEORIGIN
	Header always set X-Content-Type-Options nosniff
	SSLCompression off

	SSLCertificateFile /usr/local/sucs-pki/certs/sucs.crt
	SSLCertificateKeyFile /usr/local/sucs-pki/private/sucs.key
	SSLCertificateChainFile /usr/local/sucs-pki/certs/globalsign.crt

	<Files ~ "\.(cgi|shtml|phtml|php3?)$">
	    SSLOptions +StdEnvVars
	</Files>

	<Directory "/var/www/cgi-bin">
	    SSLOptions +StdEnvVars
	</Directory>

	SetEnvIf User-Agent ".*MSIE.*" \
		 nokeepalive ssl-unclean-shutdown \
		 downgrade-1.0 force-response-1.0

	CustomLog /var/log/apache2/ssl_request.log \
		  "%t %h %{SSL_PROTOCOL}x %{SSL_CIPHER}x \"%r\" %b"

	ProxyRequests Off
	<Proxy *>
		Order allow,deny
		Allow from all
	</Proxy>
	ProxyVia On
	SSLProxyEngine on
	ProxyPass /svn https://projects.sucs.org/svn
	<Location /svn >
		#DAV svn
		#SVNParentPath /var/projects/svn
                #Redirect / https://projects.sucs.org/
		ProxyPassReverse https://projects.sucs.org/svn
	</Location>

	Include /etc/apache2/conf.sucs.d/userdir.conf

	<Directory "/var/www/sucssite/htdocs">
		Options Indexes FollowSymLinks Includes ExecCGI MultiViews
		AllowOverride All

		XBitHack On

		Order allow,deny
		Allow from all

		Options +MultiViews
		<IfModule mod_rewrite.c>
			RewriteEngine On

			# Ensure requests are for our preferred domains
			# Disabled by ~imranh 08/04/2015
			# We want people to do to sucs.org becuase we have a ssl cert for that
			# Keep stuff thats come in via *.swan.ac.uk in .ac.uk
			#RewriteCond %{HTTP_HOST} ^www.sucs.swan.ac.uk [NC,OR]
			#RewriteCond %{HTTP_HOST} ^compsoc.swan.ac.uk [NC,OR]
			#RewriteCond %{HTTP_HOST} ^www.compsoc.swan.ac.uk [NC]
			#RewriteRule ^(.*)$ https://sucs.swan.ac.uk/$1 [R=301,L]

			#(web)mail.sucs.org to sucs.org/webmail
			RewriteCond %{HTTP_HOST} ^mail.sucs.org [NC,OR]
			RewriteCond %{HTTP_HOST} ^webmail.sucs.org [NC]
			RewriteRule ^(.*)$ https://sucs.org/webmail/$1 [R=301,L]

			# Everything else may as well goto sucs.org
			RewriteCond %{HTTP_HOST} !^sucs.org [NC]
			#RewriteCond %{HTTP_HOST} !^sucs.swan.ac.uk [NC]
			RewriteRule ^(.*)$ https://sucs.org/$1 [R=301,L]

			# If its not a file or directory pass it to the website script
			RewriteCond %{REQUEST_URI} !=/server-status
			RewriteCond %{REQUEST_FILENAME} !-f
			RewriteCond %{REQUEST_FILENAME} !-d
			RewriteCond %{REQUEST_URI} !^/index\.php.*
			RewriteRule ^(.*)$ index.php/$1 [QSA,L]
		</IfModule>

	</Directory>

	# Disabled, Moved to the gitlab VM ~imranh
	#Include /etc/apache2/conf.d/projects.conf

	# Redirect to squirrelmail install
	Alias /squirrelmail /usr/share/squirrelmail
	Alias /webmail/program/js/tiny_mce/ /usr/share/tinymce/www/
	Alias /webmail /var/lib/roundcube
	Alias /roundcube/program/js/tiny_mce/ /usr/share/tinymce/www/
	Alias /roundcube /var/lib/roundcube
	
	# 2015-03-16 Moved the /cam alias to here from /etc/apache2/conf.d/webcam to stop it becoming an alias on all virtual hosts, commented out the file in the other location. ~rjames93
	Alias /cam /var/cam
	<Directory "/var/cam">
		        Order allow,deny
		        Allow from all
		        Options Indexes SymLinksIfOwnerMatch
	</Directory>

	<Location /cam>
	        AuthType Basic
	        AuthName "Staff Only Area"
	        AuthBasicProvider ldap
	        AuthLDAPURL "ldap://127.0.0.1/dc=sucs,dc=org?uid"
	        AuthLDAPGroupAttribute memberUid
	        AuthLDAPGroupAttributeIsDN off
	        Require ldap-group cn=sucsstaff,ou=Group,dc=sucs,dc=org
	</Location>

	Alias /stats/collectd-web /var/www/collectd-web
	<Directory /var/www/collectd-web/cgi-bin>
		Options Indexes ExecCGI
                AllowOverride All
                AddHandler cgi-script .cgi
	</Directory>

</VirtualHost>
</IfModule>
