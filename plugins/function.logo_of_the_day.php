<?php
function smarty_function_logo_of_the_day($params, &$smarty)
{

    $logos = glob("../htdocs/images/awesomelogos/*.png");
    $logo_of_the_day = $logos[array_rand($logos)];
    $logo_size = getimagesize($logo_of_the_day);
    $width_height_string = $logo_size[3];

    $logo_src = str_replace("../htdocs", "", $logo_of_the_day);

    return "<img src=\"$logo_src\" $width_height_string alt=\"SUCS\" />";
}


?>
