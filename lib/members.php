<?php
define('_BASE_DN', 'ou=People,dc=sucs,dc=org');
define('_LDAP_SERVER', 'ldap://silver');

class Members
{

    private $conn; //LDAP connection

    function __construct()
    {
        // Connect and bind to ldap server
        $this->conn = ldap_connect(_LDAP_SERVER);
        ldap_set_option($this->conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        $bind = ldap_bind($this->conn);
    }

    function getMemberList()
    {
        // Search for certain members and retrieve their username and
        $search = ldap_search($this->conn, _BASE_DN, 'uid=*');
        // Sort By Username
        ldap_sort($this->conn, $search, 'uid');

        return $this->extractMember($search);
    }


    function memberView($value)
    {
        $search = ldap_search($this->conn, _BASE_DN, '(uid=' . $this->makeSafeUsername($value) . ')');
        return $this->extractMember($search);

    }

    function memberSearch($value)
    {
        $search = ldap_search($this->conn, _BASE_DN, '(|(uid=*' . $this->makeSafeUsername($value) . '*)(cn=*' . $this->makeSafeRealName($value) . '*))');
        return $this->extractMember($search);
    }


    private function extractMember($search)
    {
        // Produce an array of usernames
        $usernames = array();
        $entryHandler = ldap_first_entry($this->conn, $search);
        while ($entryHandler) {
            $username = ldap_get_values($this->conn, $entryHandler, 'uid');
            $realname = ldap_get_values($this->conn, $entryHandler, 'cn');
            $homedir = ldap_get_values($this->conn, $entryHandler, 'homedirectory');
            $usernames[] = array("uid" => $username[0], "cn" => $realname[0], "homedir" => $homedir[0], "website" => false);
            $entryHandler = ldap_next_entry($this->conn, $entryHandler);
        }
        return $usernames;
    }


// Compares two keyed arrays ( array("uid" => ?, "cn" =>) etc)
// by the last word of the "cn" field, which would seem
// to represent the surname
    private function cmpSurnames($person1, $person2)
    {
        $names1 = explode(' ', $person1['cn']);
        $names2 = explode(' ', $person2['cn']);
        return strcmp(array_pop($names1), array_pop($names2));
    }

// Compares two keyed arrays ( array("uid" => ?, "cn" =>) etc)
// by the first word (and onward) of the "cn" field, which would seem
// to represent the name
    private function cmpForenames($person1, $person2)
    {
        return strcmp($person1['cn'], $person2['cn']);
    }

// Converts a given string to something that can
// safely be used as a username to search for (although
// this doesn't necessarily mean it's a valid username).
    private function makeSafeUserName($username)
    {
        $username = trim(strtolower($username));
        return preg_replace('[^a-z0-9_]', '', $username);
    }

// Converts a given string to something that can
// safely be used as a real name to search for
    private function makeSafeRealName($username)
    {
        $username = trim(strtolower($username));
        return preg_replace('[^a-z0-9_ ]', '', $username);
    }


}

?>
