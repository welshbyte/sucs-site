<p>Mae'r gymdeithas yn darparu rhestr hir o wasanaethau i ei haelodau. Dyma rhestr o wasanaethau:</p>
<ul>
<li><a href="desktop.php">Bwrddgwaith ar Galwad</a>.</li>
<li> Ebost POP3/IMAP gyda <a href="http://spamassassin.org/">SpamAssassin</a> - Gallwch chi cael ato fe wrth <a href="https://sucs.org/webmail">Ebost Gwe Amgryptio</a></li>
<li> Safle Gwe (Mae PHP a PostgreSQL yn ar gael)</li>
<li> Lle disg i chi pan eich cyfrif llyfgell yn wedi ei llanw neu wedi ei doriad</li>
<li> Rhestri Ebost</li>
<li> <a href="/help/advisory">Gwasanaethau cynghori rhaglennu</a></li>
<li> <a href="library.php">Llyfrgell Cyfeiriad</a> o llyfr poblogaidd (including Computer Science recommended course texts)</li>
<li> Hawl gweld 24awr i ein <a href="http://sucs.org/services/room.php">ystafell gyfrifiaduron</a><br />
     Mae nodweddion yn cynnwys:<br />
       <ul>
       <li> Systemau yn wedi ei diweddaru gyda 2 cyfrifiaduron newydd (ac mwy yn dod yn fuan!)</li>
       <li> Rhywdwaith yr aelodau i cysylltu eich gliniadur</li>
       <li> Cyrchiad rhyngwyd gyflymder LAN</li>
       <li> Llosgydd CD</li>
       <li> Argraffydd laser mono ac argraffydd ffrwd incio lliwiau (am ddim o fewn rheswm)</li>
       </ul>
</li>
</ul>
