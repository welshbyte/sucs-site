

<p>Upon launching PuTTY you are presented with the session
configuration window (pictured below), which is how you tell PuTTY what
server to connect to and using which method.</p><p><img src="/pictures/puttyconfig.png" alt="puttyconfig.png" /><br /></p><p>You will need to enter yourusername@sucs.org in the host name box
and select SSH as the protocol. If you wish you can enter a name for
the connection in the saved session box and hit save (this will save
the session details for the next time you want to login).<br />
Next we start the connection by clicking Open.</p>

<p>Once the basic session is established you are prompted to enter you
password. Your password will not appear as you type it (this is normal).<br /><img src="/pictures/puttypassword.png" alt="puttypassword.png" /><br />

</p><p>As long as you get you password right within two attempts you will
see a screen with current system news at the top and the prompt at the
base:<br /><img src="/pictures/puttyloggedin.png" alt="puttyloggedin.png" /></p>

<p>If you choose to save your configuration, you should change the character encoding of your SUCS session to UTF-8. Details of how to do this can be found on the <a href="https://sucs.org/Knowledge/Help/SUCS%20Services/Using%20Milliways/Correcting%20your%20character%20encoding%20in%20Milliways">Correcting your character encoding in Milliways</a> page.</p>