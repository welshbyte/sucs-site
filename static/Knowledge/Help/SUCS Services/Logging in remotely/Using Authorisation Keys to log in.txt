






<h2>Creating Keys</h2>
         <p>If you log into other machines often, and you get fed
         up with entering your password every time you do, you want a SSH
         key which takes the place of your password<br />Below are the steps
         you require to create one on our systems</p>
         <ol>            <li>Open a terminal and type
            <pre class="console">$ ssh-keygen -t rsa</pre>
            This will generate the key pair</li>

            <li>You will be prompted where to save the new key, the
            default is what we need so just press enter</li>

            <li>Next you&#39;re asked if you wish to set a pass phrase, a short
            password used to protect the key, (this is optional for no pass phrase just hit enter)</li>

            <li>And the same pass phrase again to check for typos</li>
            <li><span class="Apple-style-span">You will now be returned to the terminal, the next step is
            to add your new key to the authorized_keys2 file.<br />As you do
            not currently have any keys, this can be done by typing the
            following command<pre class="console">$ cp .ssh/id_rsa.pub .ssh/authorized_keys2</pre>
            </span></li>

            <li>Now lets make sure it works, by logging into to silver
            <pre class="console">$ ssh silver</pre>If this does not ask you for a password, then you have done it right!</li>
</ol>

Note: If you&#39;re not doing this from a SUCS computer, you&#39;ll need to copy your key to your SUCS account. To do this, use the scp (secure copy) command. 
            <pre class="console">$ scp .ssh/id_rsa.pub your_username@sucs.org:.ssh/id_rsa.pub.tmp<br /></pre>

Then on a SUCS machine,
<pre class="console">$ cd .ssh/<br />$ cat id_rsa.pub.tmp &gt;&gt; authorized_keys2<br /></pre>
This will append your public key onto the authorized_keys2 file.
