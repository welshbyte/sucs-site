<p>Another useful command is the <b>man</b> command. If you type man followed
by another command, then the instructions for that command is displayed. The
man command uses the <b>more</b> program that you used earlier and so you use
the same keys to control this program. Try the following:
</p>

<pre>man man

man who</pre>

<p>The output from the manual program often seems daunting to the beginner, but
if you learn how to interpret it, it is very useful.</p>