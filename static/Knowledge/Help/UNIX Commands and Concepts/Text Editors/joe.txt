<p>Joe is a &quot;mode-less&quot; text editor, meaning that unlike with, for example, <a href="../../../../wiki/vi">vi</a>, you can start typing text the moment you open it. If you have ever used WordStar or one of Borland&#39;s languages, such as TurboPascal, you should find it easy to use as it uses the same control key commands.</p>

<p>To start joe, you use the command:</p>

<pre class="console">$ joe <em>[filename]</em></pre>

<p>Most commands in joe start with <em>Ctrl-K</em>. So to get the help screen, you press <em>Ctrl-K H</em>. The result is as follows:</p>

<pre class="console"><span class="reverse">   Help Screen    turn off with ^KH    more help with ESC . (^[.)              </span><br /><span class="reverse">&nbsp;</span><u>CURSOR</u>           <u>GO TO</u>            <u>BLOCK</u>      <u>DELETE</u>   <u>MISC</u>         <u>EXIT</u>      <span class="reverse">&nbsp;</span><br /><span class="reverse">&nbsp;</span>^B left ^F right ^U  prev. screen ^KB begin  ^D char. ^KJ reformat ^KX save  <span class="reverse">&nbsp;</span><br /><span class="reverse">&nbsp;</span>^P up   ^N down  ^V  next screen  ^KK end    ^Y line  ^T  options  ^C  abort <span class="reverse">&nbsp;</span><br /><span class="reverse">&nbsp;</span>^Z previous word ^A  beg. of line ^KM move   ^W &gt;word ^R  refresh  ^KZ shell <span class="reverse">&nbsp;</span><br /><span class="reverse">&nbsp;</span>^X next word     ^E  end of line  ^KC copy   ^O word&lt; ^@  insert   <u>FILE</u>      <span class="reverse">&nbsp;</span><br /><span class="reverse">&nbsp;</span><u>SEARCH</u>           ^KU top of file  ^KW file   ^J &gt;line <u>SPELL</u>        ^KE edit  <span class="reverse">&nbsp;</span><br /><span class="reverse">&nbsp;</span>^KF find text    ^KV end of file  ^KY delete ^_ undo  ^[N word     ^KR insert<span class="reverse">&nbsp;</span><br /><span class="reverse">&nbsp;</span>^L  find next    ^KL to line No.  ^K/ filter ^^ redo  ^[L file     ^KD save  <span class="reverse">&nbsp;</span><br /><span class="reverse">    IW   Unnamed                      Row 1    Col 1   12:12  Ctrl-K H for help</span></pre>

<p>Note that in the help, the symbol &quot;^&quot; represents the Ctrl key.</p>

<p>A lot of the standard <a href="../Control%20Keys">control keys</a> apply, but a few of the most commonly-used joe-specific commands are listed below:</p>

<table border="1">
<tbody><tr>
<th>Key-comb</th>
<th>Command</th>
</tr>
<tr>
<td>Ctrl-K H</td>
<td>Help</td>
</tr>
<tr>
<td>Ctrl-K D</td>
<td>Save the file</td>
</tr>
<tr>
<td>Ctrl-K X</td>
<td>Save and exit</td>
</tr>
<tr>
<td>Ctrl-K F</td>
<td>Search/Replace</td>
</tr>
<tr>
<td>Ctrl-C</td>
<td>Exit without saving</td>
</tr>
</tbody></table>
