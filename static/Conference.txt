<h2>SUCS@20 Conference <br /></h2><p>Saturday 29<sup>th</sup> November 2008, Swansea University<br /></p><p>&nbsp;To celebrate twenty years of the Swansea University Computer Society, we&#39;re having a conference.</p><p>&nbsp;</p><h3>&nbsp;Speakers</h3>

<dl>
<dt>Alan Cox (Red Hat)</dt>
<dd>Alan is a renowned Linux kernel developer and promoter of software freedom.</dd>
<dt>Robin O&#39;Leary</dt>
<dd>Robin is a founder member of Swansea University Computer Society.</dd>
<dt>Professor John Tucker (Swansea University)</dt>
<dd>Professor Tucker is the head of Computer Science at Swansea.</dd>
</dl>