<p><em>Swansea University Computer Society</em> is open to all members of the
University's Student Union. We offer a wide range of computing services to
our members, all based on Free Open Source Software (<abbr>FOSS</abbr>). Our computers
run various <a href="http://www.linux.org/"><cite>Linux</cite></a> distributions,
including <a href="http://www.fedoraproject.org"><cite>Fedora</cite></a>&nbsp;and <a href="http://www.debian.org"><cite>Debian</cite></a>. </p>
<p>Whether you're interested in
learning more about <cite>Linux</cite> and Open Source software, or simply want an
easy-to-remember email address and more space to store your coursework, <cite>SUCS</cite> is
here to help!</p>
<h3>Some of the services we offer:</h3>
<ul>
<li>Extra storage space for when your library account gets full or stops working, accessible via <a href="/dav" target="_top"><cite>WebDAV / WebFolder</cite></a></li>
<li><abbr>POP3</abbr>/<abbr>IMAP</abbr> Email account featuring <a href="http://spamassassin.org/"><cite>SpamAssassin</cite></a> - Can be accessed via <a href="webmail">Secure webmail!</a></li>
<li>Web site hosting (including <a href="http://www.php.net/"><abbr><cite>PHP</cite></abbr></a> scripting and <a href="http://www.postgresql.org/"><em>PostgreSQL</em></a> database support)</li>
<li><a href="Tools/Desktop%20on%20Demand">Desktop On Demand</a></li>
<li><a href="http://lists.sucs.org">Mailing list</a> hosting <small>(Recommended for societies who want to keep in touch with their members easily).</small></li>
<li><a href="Knowledge/Help/Program%20Advisory">Programming Advisory service</a></li>
<li><a href="Knowledge/Library">Reference Library</a> of popular books (including Computer Science recommended course texts)</li>
<li>24-hour access to our own <a href="About/Room">Computer Room</a><br />
Features include:
<ul>
<li>8 computer workstations</li>
<li>GuestNet to hook up your laptop</li>
<li><abbr>LAN</abbr> speed Internet access</li>
<li><abbr>CD</abbr>/<abbr>DVD</abbr> Burner</li>
<li>Mono Laser printing <small>(200 pages for free, 2p per page after)</small></li>
</ul>
</li>
</ul>