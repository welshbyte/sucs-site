<p><em>Note that all email addresses on this page refer to sucs.org addresses. Add </em>sucs.org<em> after the @ symbol</em></p>
<h2>The Executive</h2>
<p>If you're trying to contact the Executive team please email exec@ </p>
<p>The following people were elected to the following posts in accordance with the <a href="../About/Constitution">SUCS constitution</a>.</p>
<ul class="boxes">
<li>
<div>&nbsp;<img height="100" src="https://sucs.org/pictures/people/vectre.png" width="100" />
	<br />
	<strong>President</strong><br />Alexander Moras (~vectre)</div>
</li>
<li>
<div>&nbsp;<img height="100" src="https://sucs.org/pictures/people/elbows.png" width="100" />
	<br />
	<strong>Treasurer</strong><br />Laurence Sebastian Bowes (~elbows)</div>
</li>
<li>
<div>&nbsp;<img height="100" src="https://sucs.org/pictures/people/xray_effect.png" width="100" />
    <br />
	<strong>Secretary</strong><br />Ryan Williams (~xray_effect)</div>
</li>
<li>
<div>&nbsp;<img height="100" src="https://sucs.org/pictures/people/arcryalis.png" width="100" />
    <br />
    <strong>Gaming</strong><br />Hywel Williams (~arcryalis)</div>
</li>
</ul>
<div class="clear"></div>
<h2>Additional Staff</h2>
<p>If you're trying to contact the Admin team please email admin@</p>
<ul>
<li>Tim Clark (<a href="/~eclipse">eclipse</a>)<br /></li>
<li>Thomas Lake (<a href="/~tswsl1989/">tswsl1989</a>)</li>
<li>Imran Hussain (<a href="/~imranh/">imranh</a>)</li>
<li>Laurence Bowes (<a href="/~elbows/">elbows</a>)</li>
</ul>
<h2>Thanks</h2>
<p>
The society as a whole would like to thank the following staff for their
exceptional help over the years.</p>
<ul>
<li>Alan Cox (<a href="/~anarchy">anarchy</a>)</li>
<li>Justin Mitchell (<a href="/~arthur">arthur</a>)</li>
<li>Robin O'Leary (<a href="/~caederus">caederus</a>)</li>
<li>Denis Walker (<a href="/~dez">dez</a>)</li>
<li>Dick Porter (<a href="/~dick">dick</a>)</li>
<li>Steve Hill (<a href="/~firefury/">firefury</a>)<br /></li>
<li>Steve Whitehouse (<a href="/~rohan">rohan</a>)</li>
<li>Sitsofe Wheeler (<a href="/~sits">sits</a>)</li>
<li>Dave Arter (<a href="/~davea">davea</a>)</li>
<li>Chris Elsmore (<a href="/~elsmorian">elsmorian</a>)</li>
<li>James Frost (<a href="/~frosty">frosty</a>)</li>
<li>Peter Berry (<a href="/~pwb">pwb</a>)</li>
<li>Andrew Price (<a href="/~welshbyte">welshbyte</a>)</li>
<li>Chris Jones (<a href="/~rollercow">rollercow</a>)</li>
<li>Nicholas Corlett (<a href="/~worldinsideme">worldinsideme</a>)</li>
<li>Callum Massey (<a href="/~kais58/">kais58</a>)</li>
</ul>
<!--
<div class="box">
<div class="boxhead">
<h2><a id="contact">Contact Us</a></h2>
</div>
<div class="boxcontent">
<p>If you are having trouble with any aspect of SUCS, then feel free to contact any of the people listed below. You may want to see if your problem is covered in the <a href="../help" mce_href="../help">help pages</a> first, though. If you want an immediate response, see if any staff members are logged on to <a href="../Community/Milliways" mce_href="../Community/Milliways">Milliways</a>. During the week, there is usually someone logged on who will be able to help you.</p>
<p>If you have a problem, you can use this form to contact the relevant people</p>
<form action="http://sucs.org/people/staff.php" method="post" accept-charset="utf-8,us-ascii">
<div class="row">
		<label for="addr">To</label>
		<span class="textinput"><select name="addr" id="addr" class="input">
			<option value="" selected="selected"></option>
			<option value="admin">The Admin Team</option>
			<option value="exec">The Elected Exec.</option>
			<option value="committee">The Whole Committee</option>
			<option value="games">The Games Admin Team</option>
		</select></span>
	</div>
<div class="row">
		<label for="subject">Subject</label>
		<span class="textinput"><input type="text" name="subject" id="subject" size="60" class="input"></input></span>
		<input type="hidden" name="message" id="message" value="send"></input>
		<input type="hidden" name="msgid" id="msgid" value="Mzk3OTQ0ODU1"></input>
	</div>
<div class="row">
		<label for="from">Your email address</label>
		<span class="textinput"><input type="text" name="from" id="from" size="60" class="input"></input></span>
	</div>
<div class="row">
		<label for="body">Body</label>
		<span class="textinput"><textarea name="body" id="body" cols="70" rows="5" class="input"></textarea></span>
	</div>
<div class="row">
		<span class="textinput"><input type="submit" value="Send"></input></span>
	</div>
<div class="clear"></div>
</form>
</div>
<div class="hollowfoot">
<div>
<div></div>
</div>
</div>
</div>
-->