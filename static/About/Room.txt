<p>The SUCS room houses our network of Linux desktop PCs, along with spaces for members with laptop computers to get connected.</p>
<p><img alt="The SUCS Room" src="../pictures/sucsroom.jpg" /></p>
<h3>Where to find the room</h3>
<p style="float: left; margin: 0.5em"><a href="../pictures/room-map.jpg"><img alt="Location of SUCS Room on Campus Map" src="../pictures/room-map_thumb.jpg" /></a></p>
<p>The SUCS room is located at the bottom of the Student Union building, approximately halfway along the side facing the back of Fulton House. To unlock the door, members can hold their student card up to the <abbr>RFID</abbr> sensor pad (after first registering it with a member of the admin team) denoted by the square (both located to the right of our blue door, underneath the window).</p>