<p>
<script src="/~imranh/sucs/jwplayer/jwplayer/jwplayer.js" type="text/javascript"></script>
</p>
<p>We are currently streaming <em><strong>nothing</strong></em>.<br />We also stream student forums on behalf of the Student Union - check <a href="http://swansea-union.co.uk/">their website for dates and times</a></p>
<ul>
<div id="JWPlayer">Loading the player...</div>
<script type="text/javascript">
        jwplayer("JWPlayer").setup({
                primary: "html5",
                levels: [{
                        file: "http://137.44.10.80:8080/high.flv",
                        label: "High"
                        }
                ],
                image: ""
        });
</script>
</ul>
<p>
To view the stream in an external media player <em>(e.g VLC)</em> 
<a href="https://sucs.org/Community/Stream/External">Click Here for instructions</a><em><br /></em></p>
<p>All streams are currently FLV format. If you have any problems, please read the <a href="/Community/Stream/FAQ">FAQ</a></p>
<ul>
</ul>