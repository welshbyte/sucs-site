<div class="box">
<div class="boxhead">
<h2>Joseph Bhart (hantudemon) - "A Comparison Between Computer Science and Biochemistry"</h2>
</div>
<div class="boxcontent">
<div id="player">
<object data="/videos/talks/mediaplayer.swf?file=2012-10-18/hantudemon.flv" height="480" id="player" type="application/x-shockwave-flash" width="600">
<param name="height" value="480" />
<param name="width" value="600" />
<param name="file" value="/videos/talks/2012-10-18/hantudemon.flv" />
<param name="image" value="/videos/talks/2012-10-18/hantudemon.png" />
<param name="id" value="player" />
<param name="displayheight" value="480" />
<param name="FlashVars" value="image=/videos/talks/2012-10-18/hantudemon.png" />
</object>
</div>
<p><strong>Length: </strong>35m 17s</p>
<p><strong>Video: </strong><a href="/videos/talks/2012-10-18/hantudemon.flv" title="600x480 MPEG-4 AVC - 172MB">600x480</a> (MPEG-4 AVC, 172MB)</p>
</div>
<div class="boxfoot">
<p>&nbsp;</p>
</div>
</div>
