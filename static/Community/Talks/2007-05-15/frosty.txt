<div class="box">
	<div class="boxhead"><h2>James Frost (frosty) - &quot;Dude, What's Up With Your Keyboard?&quot;: The Dvorak Keyboard Layout</h2></div>
	<div class="boxcontent">
	<p>Have you ever really looked at a keyboard? James tells us why the keyboards most of us use every day make no sense at all, and discusses an alternative.</p>

<div id="player"><object type="application/x-shockwave-flash" data="/videos/talks/mediaplayer.swf?file=2007-05-15/2007-05-15-frosty.flv" width="320" height="260" allowscriptaccess="always" allowfullscreen="true" file="/videos/talks/2007-05-15/2007-05-15-frosty.flv" image="/videos/talks/2007-05-15/2007-05-15-frosty.png" id="player" displayheight="240"><param name="height" value="240" /><param name="width" value="320" />
<param name="file" value="/videos/talks/2007-05-15/2007-05-15-frosty.flv" />
<param name="image" value="/videos/talks/2007-05-15/2007-05-15-frosty.png" />
<param name="id" value="player" />
<param name="displayheight" value="240" />
<param name="FlashVars" value="image=/videos/talks/2007-05-15/2007-05-15-frosty.png" />
</object></div>

<p><strong>Length: </strong>14m 32s</p>
<p><strong>Video:</strong><a href="/videos/talks/2007-05-15/2007-05-15-frosty.mov" title="784x576 H.264 .mov - 104.9MB">784x576</a>(H.264 .mov, 104.9MB)</p>
<p><strong>Slides:</strong> <a href="/videos/talks/2007-05-15/2007-05-15-frosty-slides.pdf" title="Dude, What's Up With Your Keyboard?: The Dvorak Keyboard Layout">2007-05-15-frosty-slides.pdf</a> (PDF, 8.6MB)<br /></p>
</div>
	<div class="boxfoot"><p>&nbsp;</p></div>
</div>
