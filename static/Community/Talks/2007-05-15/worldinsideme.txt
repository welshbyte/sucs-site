<div class="box">
	<div class="boxhead"><h2>Nicholas Corlett (worldinsideme) - A Beginner's Guide to UNIX Commands: Part 2</h2></div>
	<div class="boxcontent">
	<p>Or: This Is Where I Ramble About UNIX Commands For 10 Minutes And Then Stop.</p>

<div id="player"><object type="application/x-shockwave-flash" data="/videos/talks/mediaplayer.swf?file=2007-05-15/2007-05-15-worldinsideme.flv" width="320" height="260" allowscriptaccess="always" allowfullscreen="true" file="/videos/talks/2007-05-15/2007-05-15-worldinsideme.flv" image="/videos/talks/2007-05-15/2007-05-15-worldinsideme.png" id="player" displayheight="240"><param name="height" value="240" /><param name="width" value="320" />
<param name="file" value="/videos/talks/2007-05-15/2007-05-15-worldinsideme.flv" />
<param name="image" value="/videos/talks/2007-05-15/2007-05-15-worldinsideme.png" />
<param name="id" value="player" />
<param name="displayheight" value="240" />
<param name="FlashVars" value="image=/videos/talks/2007-05-15/2007-05-15-worldinsideme.png" />
</object></div>

<p><strong>Length: </strong>11m 40s</p>
<p><strong>Video:</strong><a href="/videos/talks/2007-05-15/2007-05-15-worldinsideme.mov" title="784x576 H.264 .mov - 85.9MB">784x576</a>(H.264 .mov, 85.9MB98.3MB)</p>
<p><strong>Slides:</strong>Coming Soon (PDF, -)<br /></p>
</div>
	<div class="boxfoot"><p>&nbsp;</p></div>
</div>
