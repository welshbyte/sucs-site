<div class="box">
	<div class="boxhead"><h2>Sitsofe Wheeler (sits) - Bug Reporting</h2></div>
	<div class="boxcontent">
	<p>Sitsofe shares his vast experience of reporting bugs.</p>

<div id="player"><object type="application/x-shockwave-flash" data="/videos/talks/mediaplayer.swf?file=2007-11-07/2007-11-07-sits.flv" width="320" height="260" allowscriptaccess="always" allowfullscreen="true" file="/videos/talks/2007-11-07/2007-11-07-sits.flv" image="/videos/talks/2007-11-07/2007-11-07-sits.png" id="player" displayheight="240"><param name="height" value="240" /><param name="width" value="320" />
<param name="file" value="/videos/talks/2007-11-07/2007-11-07-sits.flv" />
<param name="image" value="/videos/talks/2007-11-07/2007-11-07-sits.png" />
<param name="id" value="player" />
<param name="displayheight" value="240" />
<param name="FlashVars" value="image=/videos/talks/2007-11-07/2007-11-07-sits.png" />
</object></div>

<p><strong>Length: </strong>15m 00s</p>
<p><strong>Video:</strong>Coming Soon (H.264 .mov)</p>
<p><strong>Slides:</strong>Coming Soon (PDF)<br /></p>
</div>
	<div class="boxfoot"><p>&nbsp;</p></div>
</div>
