<div class="box">
        <div class="boxhead"><h2>Chris Jones (rollercow) - &quot;An Introduction to MythTV&quot;</h2></div>
        <div class="boxcontent">
        <p>Chris Jones shows us the magic of MythTV - a system you can use to view and record TV programs, and a lot lot more.</p>

<div id="player"><object type="application/x-shockwave-flash" data="/videos/talks/mediaplayer.swf?file=2007-03-20/rollercow.flv" width="320" height="260" allowscriptaccess="always" allowfullscreen="true" file="/videos/talks/2007-03-20/welshbyte.flv" image="/videos/talks/2007-03-20/welshbyte_preview.png" id="player" displayheight="240">
<param name="height" value="240" /><param name="width" value="320" />
<param name="file" value="/videos/talks/2007-03-20/rollercow.flv" />
<param name="image" value="/videos/talks/2007-03-20/rollercow_preview.png" />
<param name="id" value="player" />
<param name="displayheight" value="240" />
<param name="FlashVars" value="image=/videos/talks/2007-03-20/rollercow_preview.png" />
</object></div>

<p><strong>Length: </strong>21m 17s</p><p><strong>Video: </strong><a href="/videos/talks/2007-03-20/2007-03-20-rollercow.ogg" title="392x288 Ogg Theora - 95.7MB">392x288</a> (Ogg Theora, 95.7MB)<br /><strong>Slides:</strong> <a href="/videos/talks/2007-03-20/2007-03-20-rollercow-slides.pdf" title="An Introduction to MythTV slides">2007-03-20-rollercow-slides.pdf</a> (PDF, 108KB)<br /></p></div>
        <div class="boxfoot"><p>&nbsp;</p></div>
</div>
