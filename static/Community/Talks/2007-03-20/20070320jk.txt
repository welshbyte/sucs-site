<div class="box">
	<div class="boxhead"><h2>James Killick (jk) - &quot;LaTeX&quot;</h2></div>
	<div class="boxcontent">
	<p>James Killick presents an introduction to the markup language LaTeX. He shows examples of the language and demonstrates how to construct a simple document.</p>

<div id="player"><object type="application/x-shockwave-flash" data="/videos/talks/mediaplayer.swf?file=2007-03-20/jk.flv" width="320" height="260" allowscriptaccess="always" allowfullscreen="true" file="/videos/talks/2007-03-20/welshbyte.flv" image="/videos/talks/2007-03-20/welshbyte.png" id="player" displayheight="240">
<param name="height" value="240" /><param name="width" value="320" />
<param name="file" value="/videos/talks/2007-03-20/jk.flv" />
<param name="image" value="/videos/talks/2007-03-20/jk_preview.png" />
<param name="id" value="player" />
<param name="displayheight" value="240" />
<param name="FlashVars" value="image=/videos/talks/2007-03-20/jk_preview.png" />
</object></div>

<p><strong>Length: </strong>10m 52s</p><p><strong>Video: </strong><a href="/videos/talks/2007-03-20/2007-03-20-jk.ogg" title="784x576 Ogg Theora - 127.3MB">784x576</a> (Ogg Theora, 127.3MB)<br /><strong>Slides:</strong> <a href="/videos/talks/2007-03-20/2007-03-20-jk-slides.pdf" title="LaTeX slides">2007-03-20-jk-slides.pdf</a> (PDF, 216KB)<br /></p>
</div>
	<div class="boxfoot"><p>&nbsp;</p></div>
</div>
