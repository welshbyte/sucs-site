<h2>SUCS Games Server</h2> 
<p>Being a member of SUCS gives you exclusive access to our games server. If you&#39;re living on campus, it&#39;s the only way to play multiplayer online games - and it&#39;s also accessible to members living off campus, too. See below for a list of the games we currently offer.</p> 

<h3>How do I play?</h3> 
<p>Before you can connect to our games server, you need to log in to our authentication system.&nbsp; Simply use the link on the right hand side of this page to log in to the system, using your SUCS username and password or your Student ID. For instructions on how to connect to a particular game, click its screenshot below.</p>
<!--
<p>You can also chat to other players by using our <a href="http://goteamspeak.com/" title="TeamSpeak 2">TeamSpeak 2</a> server. Simply <a href="https://games.sucs.org/auth/" title="Log into the SUCS games system">log in</a> to the SUCS game system, and connect to <strong>games.sucs.org:8767</strong> in TeamSpeak 2. You&#39;ll find a channel for each game that you can play.</p>
-->
<p>Steam users are also welcome to join the <a href="http://steamcommunity.com/groups/swanseauniversitycompsoc">SUCS group</a></p>

<h3>Games Currently Available</h3> 
<p>Click each game for more information, as well as links to download any addons or patches you may need.&nbsp;</p>
