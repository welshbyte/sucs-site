<p>The SUCS Room frequently accumulates large piles of junk. Some of this is no longer required by the society, and we&#39;d like to offer such items to our members before they find their way in to a skip</p>

<p>Logged-in members can request to take items from the list below. <em>We make no promises about whether any item actually works, and take no responsibility should anything you take from us lose your data / murder your cat / blow up in your face, disfiguring you horribly / bring about Armageddon / whatever.</em></p>
